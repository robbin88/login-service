var db = require('mongoose');
db.connect('mongodb://localhost/test', { useUnifiedTopology: true});
db.set('useFindAndModify', false);

module.exports = db;