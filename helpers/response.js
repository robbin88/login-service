var errorHandler = function(err, req, res, next) {
    console.log('Error Handler');
    next();
}

var responseHandler = function(req, res, next) {
    console.log('Response Handler');
    next();
}

module.exports = {errorHandler, responseHandler};