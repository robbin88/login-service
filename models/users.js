const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
const {Schema} = mongoose;

const userSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: false
    },
    contact: {
        type: String,
        required: false,
        // unique: true,
        default: ''
    },
    email: {
        type: String,
        index: true,
        unique: true,
        dropDups: true,
        required: true 
    },
    passwordHash: {
        type: String,
        required: true
    },
    registeredDate: {
        type: Date,
        required: true,
        default: Date.now()
    }
});

const Users = mongoose.model('User', userSchema);

module.exports = Users;