var express = require('express');
var router = express.Router();
var User = require('../models/users');
var bcrypt = require('bcrypt');
const saltRounds = 10;
// var errorHandler = require('../helpers/response');
// var responseHandler = require('../helpers/response');

var db = require('../helpers/db');
var database = db.connection;
database.on('error', console.error.bind(console, 'connection error:'));
database.once('open', function() {
  console.log('Connected to Database');
  // we're connected!
});
var jwt = require('jsonwebtoken');

/* GET users listing. */
router.get('/', function(req, res, next) {
  var token = req.header('X-AUTH-TOKEN');
  var decoded = jwt.decode(token);
  // res.send('respond with a resource');
  res.json({data: decoded, status: 'success'});
});
router.post('/login', function(req, res, next) {
  var postDict = req.body;
  console.log(postDict.username, postDict.password);
  User.findOne({email: postDict.username}, function(err, result) {
    console.log(result);
    if(err) {
      res.json({status: 'error', error: err});
    } else if(result) {
      var comparedHash = bcrypt.compareSync(postDict.password, result.passwordHash);
      if(comparedHash) {
        var token = jwt.sign({username: postDict.username, password: result.passwordHash}, 'some secret');
        res.json({status: 'success', data: result, token: token});
      } else {
        res.json({status: 'failed', reason: 'Wrong Password'});
      }
    } else {
      res.json({status: 'failed', reason: 'No User found'});
    }
  });
})
router.post('/register', function(req, res, next) {
  var postDict = req.body;
  const passwordHash = bcrypt.hashSync(postDict.password, saltRounds);
  var userData = {
    username: postDict.username,
    passwordHash: passwordHash,
    email: postDict.email,
    contact: postDict.contact
  };
  console.log(userData);
  var data = new User(userData);
  data.save().then(item => {
    var token = jwt.sign(userData, 'some secret');
    res.json({status: 'success', storedItem: item, token: token});
  }).catch(err => {
    res.json({status: 'failed', error: err, data: postDict});
  });
})

module.exports = router;
